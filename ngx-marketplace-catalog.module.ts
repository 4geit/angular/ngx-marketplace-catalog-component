import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxMarketplaceCatalogComponent } from './ngx-marketplace-catalog.component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxMaterialModule,
  ],
  declarations: [
    NgxMarketplaceCatalogComponent
  ],
  exports: [
    NgxMarketplaceCatalogComponent
  ]
})
export class NgxMarketplaceCatalogModule { }
