import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxMarketplaceCatalogComponent } from './ngx-marketplace-catalog.component';

describe('marketplace-catalog', () => {
  let component: marketplace-catalog;
  let fixture: ComponentFixture<marketplace-catalog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ marketplace-catalog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(marketplace-catalog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
