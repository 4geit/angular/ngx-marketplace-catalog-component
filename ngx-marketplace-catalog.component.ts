import { Component, OnInit } from '@angular/core';

import { NgxMarketplaceProductsService } from '@4geit/ngx-marketplace-products-service';

@Component({
  selector: 'ngx-marketplace-catalog',
  template: require('pug-loader!./ngx-marketplace-catalog.component.pug')(),
  styleUrls: ['./ngx-marketplace-catalog.component.scss']
})
export class NgxMarketplaceCatalogComponent implements OnInit {

  constructor(
    private productsService: NgxMarketplaceProductsService
  ) { }

  ngOnInit() {
  }

}
